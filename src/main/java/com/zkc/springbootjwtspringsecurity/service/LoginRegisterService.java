package com.zkc.springbootjwtspringsecurity.service;

import com.zkc.springbootjwtspringsecurity.dto.UserCO;
import com.zkc.springbootjwtspringsecurity.dto.UserDO;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-10
 */
public interface LoginRegisterService {

    boolean register(UserCO userCO);

    String login(String userName, String passWord);

}