package com.zkc.springbootjwtspringsecurity.service;

import com.zkc.springbootjwtspringsecurity.dto.UserDO;

import java.util.List;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-10
 */
public interface UserService {

    List<UserDO> listUser();

    UserDO getUserById(Long uid);

}