package com.zkc.springbootjwtspringsecurity.service.impl;

import com.zkc.springbootjwtspringsecurity.dto.UserCO;
import com.zkc.springbootjwtspringsecurity.dto.UserDO;
import com.zkc.springbootjwtspringsecurity.enums.UserEnableEnum;
import com.zkc.springbootjwtspringsecurity.mapper.UserMapper;
import com.zkc.springbootjwtspringsecurity.service.LoginRegisterService;
import com.zkc.springbootjwtspringsecurity.util.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-10
 */
@Service
@RequiredArgsConstructor
public class LoginRegisterServiceImpl implements LoginRegisterService {

    private final UserMapper userMapper;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;


    @Override
    public boolean register(UserCO userCO) {
        UserDO userDO = new UserDO();
        userDO.setUserName(userCO.getUserName());
        PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        userDO.setPassWord(passwordEncoder.encode(userCO.getPassWord()));
        userDO.setCreateTime(LocalDateTime.now());
        userDO.setEnabled(UserEnableEnum.ENABLE.getCode());
        return userMapper.insert(userDO) == 1 ? true : false;
    }

    @Override
    public String login(String userName, String passWord) {
        // 创建Spring Security登录token
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userName, passWord);
        // 委托Spring Security认证组件执行认证过程
        Authentication authentication = authenticationManager.authenticate(authenticationToken);
        // 认证成功，设置上下文
        SecurityContextHolder.getContext().setAuthentication(authentication);
        // 生成jwt token
        return jwtTokenUtil.generateToken(userName);
    }

}