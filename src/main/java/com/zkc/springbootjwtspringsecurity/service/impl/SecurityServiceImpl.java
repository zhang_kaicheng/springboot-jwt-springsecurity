package com.zkc.springbootjwtspringsecurity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.zkc.springbootjwtspringsecurity.dto.SecurityUser;
import com.zkc.springbootjwtspringsecurity.dto.UserDO;
import com.zkc.springbootjwtspringsecurity.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-04
 */
@Service
@RequiredArgsConstructor
public class SecurityServiceImpl implements UserDetailsService {

    private final UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        LambdaQueryWrapper<UserDO> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(UserDO::getUserName, userName);
        UserDO userDO = userMapper.selectOne(queryWrapper);
        if (userDO != null) {
            return new SecurityUser(userName, userDO.getPassWord(), new ArrayList<>(), userDO.getEnabled());
        } else {
            throw new UsernameNotFoundException(String.format("%s 该账号不存在", userName));
        }
    }

}