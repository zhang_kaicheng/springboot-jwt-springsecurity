package com.zkc.springbootjwtspringsecurity.dto;

import com.zkc.springbootjwtspringsecurity.enums.ResponseCodeEnum;
import lombok.Data;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-10
 */
@Data
public final class ResponseData<T> {

    private Integer status = ResponseCodeEnum.SUCCESS.getCode();

    private T data;


    /**
     * 返回结果描述
     */
    private String message;

    private String error;

    public ResponseData(T data) {
        this.data = data;
    }

    public ResponseData() {
    }

    public static <T> ResponseData<T> builderSuccess(T data) {
        return new ResponseData(data);
    }

    public static ResponseData builderFailed(Integer status, String msg) {
        ResponseData responseData = new ResponseData();
        responseData.setStatus(status);
        responseData.setMessage(msg);
        return responseData;
    }

}