package com.zkc.springbootjwtspringsecurity.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-04
 */
@Data
@TableName(value = "t_user")
public class UserDO {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String userName;
    private String passWord;
    private Integer enabled;
    private LocalDateTime createTime;

}