package com.zkc.springbootjwtspringsecurity.enums;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-10
 */
public enum UserEnableEnum {

    ENABLE(0, "可用"),
    NO_ENABLE(1, "不可用");

    private Integer code;
    private String msg;

    UserEnableEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}