package com.zkc.springbootjwtspringsecurity.enums;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-10
 */
public enum ResponseCodeEnum {

    SUCCESS(200, "成功"),
    ERROR(500, "失败"),
    AUTHENTICATION_FAIL(401, "认证失败"),
    ACCESS_FAIL(403, "授权失败");

    private Integer code;
    private String msg;

    ResponseCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}