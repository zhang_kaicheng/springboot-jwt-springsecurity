package com.zkc.springbootjwtspringsecurity.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zkc.springbootjwtspringsecurity.dto.UserDO;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-04
 */
public interface UserMapper extends BaseMapper<UserDO> {
}