package com.zkc.springbootjwtspringsecurity.util;

import com.alibaba.fastjson.JSON;
import com.zkc.springbootjwtspringsecurity.dto.ResponseData;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-10
 */
public class ResponseOut {

    public static void out(HttpServletResponse response, Integer status, String msg) {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json; charset=utf-8");
        try (PrintWriter out = response.getWriter()) {
            out.println(JSON.toJSONString(ResponseData.builderFailed(status, msg)));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}