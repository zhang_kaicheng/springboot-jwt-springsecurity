package com.zkc.springbootjwtspringsecurity.controller;

import com.zkc.springbootjwtspringsecurity.dto.ResponseData;
import com.zkc.springbootjwtspringsecurity.dto.UserCO;
import com.zkc.springbootjwtspringsecurity.service.LoginRegisterService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-10
 */
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class LoginRegisterController {

    private final LoginRegisterService loginRegisterService;

    @PostMapping("/register")
    public ResponseData register(@Validated UserCO userCO) {
        return ResponseData.builderSuccess(loginRegisterService.register(userCO));
    }

    @PostMapping("/login")
    public ResponseData login(@Validated UserCO userCO) {
        return ResponseData.builderSuccess(loginRegisterService.login(userCO.getUserName(), userCO.getPassWord()));
    }


}