package com.zkc.springbootjwtspringsecurity.controller;

import com.zkc.springbootjwtspringsecurity.dto.ResponseData;
import com.zkc.springbootjwtspringsecurity.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-10
 */
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class DemoController {

    private final UserService userService;

    @GetMapping("/userList")
    public ResponseData listUser(){
        return ResponseData.builderSuccess(userService.listUser());
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/user/{uid}")
    public ResponseData getUserById(@PathVariable("uid") Long uid){
        return ResponseData.builderSuccess(userService.getUserById(uid));
    }

}