package com.zkc.springbootjwtspringsecurity.config;

import com.zkc.springbootjwtspringsecurity.enums.ResponseCodeEnum;
import com.zkc.springbootjwtspringsecurity.util.ResponseOut;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-09
 */
@Component
public class AuthenticationFailedStrategy implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        ResponseOut.out(response, ResponseCodeEnum.AUTHENTICATION_FAIL.getCode(), e.getMessage());
    }

}