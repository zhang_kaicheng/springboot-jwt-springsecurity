package com.zkc.springbootjwtspringsecurity.config;

import com.zkc.springbootjwtspringsecurity.enums.ResponseCodeEnum;
import com.zkc.springbootjwtspringsecurity.util.ResponseOut;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author kczhang@wisedu.com
 * @version 1.0.0
 * @since 2020-12-09
 */
@Component
public class AccessFailedStrategy implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
        ResponseOut.out(response, ResponseCodeEnum.ACCESS_FAIL.getCode(), e.getMessage());
    }

}